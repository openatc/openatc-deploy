# -----------------
# postgresql
# -----------------
# create init scripts
kubectl create configmap pgdata-initdb  --from-file=../data-vol/pgdata/initdb
# create password secret
kubectl create secret generic postgresql-secret --from-file=./pg-pass
# do deployment
# create pods and svc
kubectl replace --force -f postgresql.yml
# -----------------
# redis
# -----------------
# config file
kubectl create configmap redis-config  --from-file=../data-vol/redis
# do deployment
kubectl replace --force -f redis.yml

# -----------------
# openatc-admin
# -----------------
kubectl replace --force -f openatc-admin.yml

# -----------------
# openatc-vatc
# -----------------
kubectl create configmap vatc-config  --from-file=../data-vol/vatc-config
kubectl replace --force -f vatc.yml