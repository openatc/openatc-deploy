# 运维环境部署手册

## 1 环境要求
* 如果部署在内网环境服务中还需要准备一台公网环境的服务器用于下载镜像
* 如果部署在公网环境服务器中只需要公网环境服务器即可，所有操作都在公网环境服务器上进行


## 2 内网环境服务器安装docker
&emsp;官网下载docker二进制包，推荐下载docker-20.10.21.tgz 
* https://download.docker.com/linux/static/stable/x86_64/

&emsp;上传docker-20.10.21.tgz到/usr/local目录下

&emsp;解压docker-20.10.21.tgz
* cd /usr/local
* tar -zxvf docker-20.10.21.tgz

&emsp;将解压后的文件复制到/usr/bin目录下
* cp /usr/local/docker/* /usr/bin

&emsp;创建docker.service文件，用于docker服务开机自启
* vi /etc/systemd/system/docker.service
文件内容如下：
```
[Unit]
Description=Docker Application Container Engine
Documentation=https://docs.docker.com
After=network-online.target firewalld.service
Wants=network-online.target
[Service]
Type=notify
ExecStart=/usr/bin/dockerd --selinux-enabled=false --insecure-registry=127.0.0.1
ExecReload=/bin/kill -s HUP $MAINPID
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity
TimeoutStartSec=0
Delegate=yes
KillMode=process
Restart=on-failure
StartLimitBurst=3
StartLimitInterval=60s
[Install]
WantedBy=multi-user.target
```

&emsp;设置文件权限
* chmod 777 /etc/systemd/system/docker.service


&emsp;重新加载配置
* systemctl daemon-reload 

&emsp;启动docker
* systemctl start docker

&emsp;新建daemon.json
* vi /etc/docker/daemon.json
文件内容如下
```
{
    "log-opts": {
        "max-size": "10m"
    }
}
```

&emsp;设置开机自启动
* systemctl enable docker.service

&emsp;重启docker
* systemctl daemon-reload 
* systemctl restart docker

&emsp;查看docker服务状态
* systemctl status docker
```
[root@localhost local]# systemctl status docker
● docker.service - Docker Application Container Engine
   Loaded: loaded (/etc/systemd/system/docker.service; enabled; vendor preset: disabled)
   Active: active (running) since 三 2023-04-12 15:44:01 CST; 4s ago   # 显示Active: active (running) since正在运行
     Docs: https://docs.docker.com
 Main PID: 9722 (dockerd)
    Tasks: 17
   Memory: 37.6M
   CGroup: /system.slice/docker.service
           ├─9722 /usr/bin/dockerd --selinux-enabled=false --insecure-registry=127.0.0.1
           └─9728 containerd --config /var/run/docker/containerd/containerd.toml --log-level info
```

&emsp;查看docker版本
* docker -v
```
[root@localhost local]# docker -v
Docker version 20.10.21, build baeda1f            # 版本为20.10.21
```

## 3 获取镜像
&emsp;在已安装docker并且是公网环境的服务器中下载镜像
* docker pull portainer/portainer-ce:2.11.1-alpine
* docker pull opeantc/openatc-admin
* docker pull redis:6.0
* docker pull postgres:14

&emsp;将镜像打成tar包存放在/home/tar目录下
* mkdir -p /home/tar
* cd /home/tar/
* docker save -o openatc.tar redis:6.0 portainer/portainer-ce:2.11.1-alpine postgres:14 openatc/openatc-admin:latest

&emsp;下载/home/tar目录下的openatc.tar

## 4 导入镜像
&emsp;在内网环境服务器中新建/home/tar目录
* mkdir /home/tar

&emsp;上传openatc.tar到/home/tar

&emsp;导入镜像
* cd /home/tar/
* docker load -i openatc.tar
```
[root@localhost tar]# docker load -i openatc.tar                         #成功
8d3ac3489996: Loading layer [==================================================>]  5.866MB/5.866MB
7018159c844d: Loading layer [==================================================>]  279.5MB/279.5MB
Loaded image: portainer/portainer-ce:2.11.1-alpine                               #成功
3af14c9a24c9: Loading layer [==================================================>]     84MB/84MB
a9fa3183c595: Loading layer [==================================================>]  10.19MB/10.19MB
3a54f275df41: Loading layer [==================================================>]    340kB/340kB
0188104e1297: Loading layer [==================================================>]  4.259MB/4.259MB
7bde5bee9d2d: Loading layer [==================================================>]   25.7MB/25.7MB
ef59638c463e: Loading layer [==================================================>]  3.554MB/3.554MB
96a5e612e773: Loading layer [==================================================>]  2.048kB/2.048kB
5edf3610ca63: Loading layer [==================================================>]  8.704kB/8.704kB
1aad6d181250: Loading layer [==================================================>]  256.3MB/256.3MB
73eaa982873a: Loading layer [==================================================>]  66.56kB/66.56kB
fa997a6fd9c4: Loading layer [==================================================>]  2.048kB/2.048kB
2ad0869ba131: Loading layer [==================================================>]  3.584kB/3.584kB
3377d4004edc: Loading layer [==================================================>]  15.87kB/15.87kB
Loaded image: postgres:14                               #成功
9c742cd6c7a5: Loading layer [==================================================>]  129.2MB/129.2MB
03127cdb479b: Loading layer [==================================================>]   11.3MB/11.3MB
293d5db30c9f: Loading layer [==================================================>]  19.31MB/19.31MB
9b55156abf26: Loading layer [==================================================>]  156.5MB/156.5MB
b626401ef603: Loading layer [==================================================>]  11.74MB/11.74MB
826c3ddbb29c: Loading layer [==================================================>]  3.584kB/3.584kB
7b7f3078e1db: Loading layer [==================================================>]  337.8MB/337.8MB
77f0481dc189: Loading layer [==================================================>]  3.072kB/3.072kB
2b2fe397fbc4: Loading layer [==================================================>]  73.31MB/73.31MB
Loaded image: openatc/openatc-admin:latest                               #成功
79f41069ccc1: Loading layer [==================================================>]  338.4kB/338.4kB
2cf793f55ead: Loading layer [==================================================>]  4.227MB/4.227MB
fa7295112cc6: Loading layer [==================================================>]  26.82MB/26.82MB
017b6cbf6a2d: Loading layer [==================================================>]  2.048kB/2.048kB
0bd166ccd4ad: Loading layer [==================================================>]  4.096kB/4.096kB
Loaded image: redis:6.0                               #成功
```

&emsp;查看镜像
* docker images
```
[root@localhost tar]# docker images
REPOSITORY               TAG             IMAGE ID       CREATED         SIZE
openatc/openatc-admin    latest          a231c315fe23   5 days ago      727MB
postgres                 14              54b4b5f5550e   2 weeks ago     377MB
redis                    6.0             77adb12aae5f   2 weeks ago     112MB
portainer/portainer-ce   2.11.1-alpine   f32b21b1077b   14 months ago   285MB
```

## 5 安装portainer
&emsp;新建/home/portainer-data
* mkdir -p /home/data-vol/portainer-data
* chmod 755 /home/data-vol/portainer-data

&emsp;执行
docker run -d -p 9000:9000 --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v /home/data-vol/portainer-data:/data --name prtainer portainer/portainer-ce:2.11.1-alpine

```
[root@localhost portainer]# docker run -d -p 9000:9000 --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v /home/data-vol/portainer-data:/data --name prtainer portainer/portainer-ce:2.11.1-alpine
e4c8aa45374a73859ca6ad31f9f3e76e76b2c31f681429a89135fe8dd3a82b5e                      #执行成功
```

&emsp;成功后，可以远程通过 服务器IP:9000 访问Portainer 管理界面

&emsp;根据提示设置密码
![portainer设置密码](imgs/1-5.png)


## 6 部署openatc
&emsp;新建/home/data-vol/redisdata、/home/data-vol/radisconf
* mkdir -p /home/data-vol/redisdata
* mkdir -p /home/data-vol/redisconf

&emsp;新建redis.conf
* vi /home/data-vol/redisconf/redis.conf
内容如下：
```
# Redis configuration file example.
 
####### Main configuration start #######
 
#注释掉bind 127.0.0.1，使redis可以外部访问
#bind 127.0.0.1
 
# 端口号
port 6379
 
#给redis设置密码,生成环境建议使用复杂的密码
requirepass password
 
##redis持久化　　默认是no
appendonly yes
 
#开启protected-mode保护模式，需配置bind ip或者设置访问密码
#关闭protected-mode模式，此时外部网络可以直接访问
protected-mode yes
 
#是否开启集群
#cluster-enabled no
 
#集群的配置文件,该文件自动生成
#cluster-config-file nodes.conf
 
#集群的超时时间
#cluster-node-timeout 5000
 
#用守护线程的方式启动
daemonize no   
 
#防止出现远程主机强迫关闭了一个现有的连接的错误 默认是300
tcp-keepalive 300
 
####### Main configuration end #######
timeout 0
tcp-backlog 511
supervised no
pidfile /var/run/redis_6379.pid
loglevel notice
logfile ""
databases 16
always-show-logo yes 
save 900 1
save 300 10
save 60 10000
stop-writes-on-bgsave-error yes
rdbcompression yes
rdbchecksum yes
dbfilename dump.rdb
rdb-del-sync-files no
dir ./
replica-serve-stale-data yes
replica-read-only yes
repl-diskless-sync no
repl-diskless-sync-delay 5
repl-diskless-load disabled
repl-disable-tcp-nodelay no
replica-priority 100
acllog-max-len 128
lazyfree-lazy-eviction no
lazyfree-lazy-expire no
lazyfree-lazy-server-del no
replica-lazy-flush no
lazyfree-lazy-user-del no
appendfilename "appendonly.aof"
appendfsync everysec
no-appendfsync-on-rewrite no
auto-aof-rewrite-percentage 100
auto-aof-rewrite-min-size 64mb
aof-load-truncated yes
aof-use-rdb-preamble yes
lua-time-limit 5000
slowlog-log-slower-than 10000
slowlog-max-len 128
latency-monitor-threshold 0
notify-keyspace-events ""
hash-max-ziplist-entries 512
hash-max-ziplist-value 64
list-max-ziplist-size -2
list-compress-depth 0
set-max-intset-entries 512
zset-max-ziplist-entries 128
zset-max-ziplist-value 64
hll-sparse-max-bytes 3000
stream-node-max-bytes 4096
stream-node-max-entries 100
activerehashing yes
client-output-buffer-limit normal 0 0 0
client-output-buffer-limit replica 256mb 64mb 60
client-output-buffer-limit pubsub 32mb 8mb 60
hz 10
dynamic-hz yes
aof-rewrite-incremental-fsync yes
rdb-save-incremental-fsync yes
jemalloc-bg-thread yes
```
&emsp;在portainer中新建stack
![新建stack-1](imgs/1-6.png)
![新建stack-2](imgs/1-7.png)
![新建stack-3](imgs/1-8.png)
![新建stack-4](imgs/1-9.png)

&emsp;将下方内容拷贝到编辑框
```
version: "3.7"
services:

  postgresql:
    image: 'postgres:14'      
    volumes:
      - postgresql-data:/var/lib/postgresql/data
    ports:
      - '5432:5432'       
    environment:
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=password

  redis:
    image: 'redis:6.0'     
    ports:
      - '6379:6379'
    command: redis-server /etc/redis/redis.conf
    volumes:
      - /home/data-vol/redisdata/:/data
      - /home/data-vol/redisconf/redis.conf:/etc/redis/redis.conf

          
  openatc:
    image: 'openatc/openatc-admin:latest'
    ports:
      - mode: host
        protocol: tcp
        published: 10003
        target: 10003

      - '31003:31003/udp'
      - '31002:31002/udp'

    environment:
      - ppostgresql=postgresql
      - ppostgresport=5432
      - pgusername=postgres
      - pgpassword=password
      - predis=redis
      - predisport=6379
      - predispassword=password

volumes:
  postgresql-data:
```
![新建stack-5](imgs/1-10.png)

&emsp;点击Deploy the stack启动服务
![新建stack-6](imgs/1-11.png)

&emsp;进入portainer管理界面新建openatc数据库
![新建数据库 ](imgs/1-2.png)
![图1‑3 ](imgs/1-3.png)
* ```su – postgres```      
* ```psql```            
* ```create database openatc;```     
![图1‑1 ](imgs/1-1.png)

&emsp;数据库创建完成后，选择stack中的openatc服务，点击restart重新启动服务。
![图1‑4 ](imgs/1-4.png)

&emsp;成功后在浏览器地址栏中，输入 服务器IP:10003/openatc ,正常显示设置密码界面平台安装成功
![图1‑5 ](imgs/设置密码.png)